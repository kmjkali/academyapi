package com.kmj.academyapi.controller;

import com.kmj.academyapi.model.StudentRequest;
import com.kmj.academyapi.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor //누군가가 필요합니다
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;
    @PostMapping("/new")
    public String setStudent(@RequestBody StudentRequest request) {
        studentService.setStudent(request);
        return "OK";
    }
}
